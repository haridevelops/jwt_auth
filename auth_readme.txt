1. install mysql 8+
2. create database using below command
	create database moviedb
	character set utf8;
3. install postman
4. register an user using the post request in postman
URL: http://localhost:8089/user/register
request : JSON object
{
	"userId": "harisudhan",
	"firstName": "harisudhan",
	"lastName": "manivannan",
	"password": "password1"
}

response: String response (Success message or failure message)
5. login using the below url in postman
URL: http://localhost:8089/user/login
request: JSON Object
{
	"userId": "harisudhan",
	"password": "password1"
}

response: JSON object
{
    "message": "User Log in Successful",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJoYXJpc3VkaGFuIiwiaWF0IjoxNTQ1MzIzMTUyfQ.hAQRfBq41A-YZJkLGpY7UJ-3MqCCBiDa-J_SZe0uUMg"
}