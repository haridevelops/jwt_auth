package com.haridevelops.moviecruiserapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class MovieCruiserAuthenticationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieCruiserAuthenticationServiceApplication.class, args);
	}
}
